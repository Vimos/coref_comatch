import os

from nltk.tokenize import sent_tokenize, word_tokenize


def sent_word_tokenize(text):
    sents = sent_tokenize(text)
    words = [word_tokenize(s) for s in sents]
    return words


def preprocess(task):
    print('Preprocessing the dataset ' + task + '...')
    sequence_dir = os.path.join('data', task, 'sequence')
    if not os.path.isdir(sequence_dir):
        os.makedirs(sequence_dir)

    if task == 'race':
        from race.preprocess import preprocess
        preprocess()
    elif task == 'multirc':
        from multirc.preprocess import preprocess
        preprocess()
    else:
        raise Exception("Task {} is not supported".format(task))


if __name__ == '__main__':
    import sys
    preprocess(sys.argv[1])
