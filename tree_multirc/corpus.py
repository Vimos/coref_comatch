import json
import os
from collections import namedtuple

import torch
from nltk import Tree
from nltk.tokenize.moses import MosesDetokenizer

detokenizer = MosesDetokenizer()
file_path = os.path.dirname(__file__)


def prep_glove():
    vocab = {}
    ivocab = []
    tensors = []
    with open('/home/vimos/Data/Dataset/glove/glove.840B.300d.txt', 'r', encoding='utf8') as f:
        for line in f:
            vals = line.rstrip().split(' ')
            if len(vals) != 301:
                print(line)
                continue
            assert (len(vals) == 301)
            word = vals[0]
            vec = torch.FloatTensor([float(v) for v in vals[1:]])
            vocab[word] = len(ivocab)
            ivocab.append(word)
            tensors.append(vec)
            assert (vec.size(0) == 300)
    assert len(tensors) == len(ivocab)
    tensors = torch.cat(tensors).view(len(ivocab), 300)
    with open('data/glove/glove_emb.pt', 'wb') as fpw:
        torch.save([tensors, vocab, ivocab], fpw)


class Dictionary(object):
    def __init__(self, task):
        self.task = task
        filename = os.path.join('data', self.task, 'word2idx.pt')

        if os.path.exists(filename):
            self.word2idx = torch.load(os.path.join('data', self.task, 'word2idx.pt'))
            self.idx2word = torch.load(os.path.join('data', self.task, 'idx2word.pt'))
            self.word2idx_count = torch.load(os.path.join('data', self.task, 'word2idx_count.pt'))

            self.char2idx = torch.load(os.path.join('data', self.task, 'char2idx.pt'))
            self.idx2char = torch.load(os.path.join('data', self.task, 'idx2char.pt'))
            self.char2idx_count = torch.load(os.path.join('data', self.task, 'char2idx_count.pt'))

            self.label_dict = torch.load(os.path.join('data', self.task, 'label_dict.pt'))
        else:
            self.word2idx = {'<<padding>>': 0, '<<unk>>': 1}
            self.word2idx_count = {'<<padding>>': 0, '<<unk>>': 0}
            self.idx2word = ['<<padding>>', '<<unk>>']

            self.char2idx = {'<<padding>>': 0, '<<unk>>': 1}
            self.char2idx_count = {'<<padding>>': 0, '<<unk>>': 0}
            self.idx2char = ['<<padding>>', '<<unk>>']

            self.label_dict = {}

            self.build_dict('train')
            self.build_dict('test')

            torch.save(self.word2idx, os.path.join('data', self.task, 'word2idx.pt'))
            torch.save(self.idx2word, os.path.join('data', self.task, 'idx2word.pt'))
            torch.save(self.word2idx_count, os.path.join('data', self.task, 'word2idx_count.pt'))

            torch.save(self.char2idx, os.path.join('data', self.task, 'char2idx.pt'))
            torch.save(self.idx2char, os.path.join('data', self.task, 'idx2char.pt'))
            torch.save(self.char2idx_count, os.path.join('data', self.task, 'char2idx_count.pt'))

            torch.save(self.label_dict, os.path.join('data', self.task, 'label_dict.pt'))

        filename_emb = os.path.join('data', task, 'embeddings.pt')
        if os.path.exists(filename_emb):
            self.embs = torch.load(filename_emb)
        else:
            self.embs = self.build_emb()

        print("vacabulary size: " + str(len(self.idx2word)))

    def add_char(self, char):
        if char not in self.char2idx:
            self.char2idx[char] = len(self.idx2char)
            self.idx2char.append(char)

            self.char2idx_count[char] = 1
        else:
            self.char2idx_count[char] += 1

        return self.char2idx[char]

    def add_word(self, word):
        if word not in self.word2idx:
            self.word2idx[word] = len(self.idx2word)
            self.idx2word.append(word)

            self.word2idx_count[word] = 1
        else:
            self.word2idx_count[word] += 1

        return self.word2idx[word]

    def build_dict(self, dataset):
        filename = os.path.join('data', self.task, 'sequence', dataset + '.json')

        if self.task in ['resources']:
            with open(filename, 'r', encoding='utf-8') as fpr:
                data_all = json.load(fpr)
                for instance in data_all:
                    for s in instance["question"]["sentences"]:
                        for t in s['tokens']:
                            self.add_word(t['word'])
                    tree = Tree.fromstring(instance["tree"])
                    for p in tree.treepositions():
                        if isinstance(tree[p], Tree):
                            for c in tree[p].label():
                                self.add_char(c)
                            self.label_dict.setdefault(tree[p].label(), 0)
                            self.label_dict[tree[p].label()] += 1
        else:
            assert False, 'the task ' + self.task + ' is not supported!'

    def build_emb(self, all_vacob=False, filter=False, threshold=10):
        word2idx = torch.load(os.path.join('data', self.task, 'word2idx.pt'))
        idx2word = torch.load(os.path.join('data', self.task, 'idx2word.pt'))
        emb = torch.FloatTensor(len(idx2word), 300).zero_()
        print("Loading Glove ...")
        print("Raw vacabulary size: " + str(len(idx2word)))

        if not os.path.exists('data/glove/glove_emb.pt'):
            prep_glove()
        glove_tensors, glove_vocab, glove_ivocab = torch.load('data/glove/glove_emb.pt')

        if not all_vacob:
            self.word2idx = {'<<padding>>': 0, '<<unk>>': 1}
            self.idx2word = ['<<padding>>', '<<unk>>']
        count = 0
        for w_id, word in enumerate(idx2word):
            if word in glove_vocab:
                id = self.add_word(word)
                emb[id] = glove_tensors[glove_vocab[word]]
                count += 1
        emb = emb[:len(self.idx2word)]

        print("Number of words not appear in glove: " + str(len(idx2word) - count))
        print("Vacabulary size: " + str(len(self.idx2word)))
        torch.save(emb, os.path.join('data', self.task, 'embeddings.pt'))
        torch.save(self.word2idx, os.path.join('data', self.task, 'word2idx.pt'))
        torch.save(self.idx2word, os.path.join('data', self.task, 'idx2word.pt'))

        return emb

    def filter(self, threshold=10):
        for word, count in self.word2idx_count.items():
            if count > threshold and word not in self.word2idx:
                self.word2idx[word] = len(self.idx2word)
                self.idx2word.append(word)

    def __len__(self):
        return len(self.idx2word)


class Corpus(object):
    def __init__(self, task, device):
        self.task = task
        self.dictionary = Dictionary(task)
        self.device = device

        self.data_all, self.start_id, self.indices = {}, {}, {}
        setnames = ['train', 'test']
        for setname in setnames:
            self.data_all[setname] = self.load_data(setname)
            print(setname, len(self.data_all[setname]))
            self.start_id[setname] = 0
            self.indices[setname] = torch.randperm(len(self.data_all[setname])) if setname == 'train' else torch.arange(
                0, len(self.data_all[setname]))

    @staticmethod
    def load_data(data_type):
        with open(os.path.join(resources_dir, 'sequence', data_type) + '.json', 'r', encoding='utf-8') as fpw:
            data_all = json.load(fpw)
        return data_all

    def batch_tree(self, tree_list):
        data = {}
        position_set = set({})
        node_type = {}
        label_max_len = max([len(dd) for dd in self.dictionary.label_dict.keys()])
        for i, t in enumerate(tree_list):
            for p in t.treepositions('postorder'):
                position_set.add(p)
                node_type.setdefault(p, [])
                node_type[p].append(type(t[p]))

                data.setdefault(p, [torch.zeros(len(tree_list), label_max_len).to(self.device),
                                    torch.zeros(len(tree_list)).to(self.device)])
                if isinstance(t[p], Tree):
                    for j, c in enumerate(t[p].label()):
                        data[p][0][i, j] = self.dictionary.char2idx.get(c, 1)
                else:
                    pp, idx = p[:-1], p[-1]
                    data.setdefault(pp, [torch.zeros(len(tree_list), label_max_len).to(self.device),
                                         torch.zeros(len(tree_list)).to(self.device)])
                    data[pp][1][i] = self.dictionary.word2idx.get(t[p], 1)

        bt = Tree('ROOT', [])
        for p in sorted(position_set, key=len):
            if p == ():
                continue
            else:
                pp, idx = p[:-1], p[-1]
                all_str = True
                for t in node_type[p]:
                    if t is not str:
                        all_str = False
                if not all_str:
                    bt[pp].insert(idx, Tree(data[p], []))
        return bt

    def get_batch(self, batch_size, setname):
        if self.start_id[setname] >= len(self.data_all[setname]):
            self.start_id[setname] = 0
            if setname == 'train':
                self.indices[setname] = torch.randperm(len(self.data_all[setname]))

        end_id = self.start_id[setname] + batch_size if self.start_id[setname] + batch_size < len(
            self.data_all[setname]) else len(self.data_all[setname])

        batch_record = namedtuple('BatchRecord', 'qids, batch_tree, labels, positions, trees, mentions, masks')
        qids, trees, labels, mentions = [], [], [], []
        for i in range(self.start_id[setname], end_id):
            instance_id = int(self.indices[setname][i])

            instance = self.data_all[setname][instance_id]
            qids.append(instance['qid'])
            trees.append(Tree.fromstring(instance['tree']))
            labels.append(instance['labels'])
            mentions.append(instance['mentions'])

        self.start_id[setname] += batch_size

        bt = self.batch_tree(trees)

        batch_record.qids = qids
        batch_record.trees = trees
        batch_record.mentions = mentions
        batch_record.batch_tree = bt
        batch_record.positions = bt.treepositions('postorder')[:-1]

        logits = torch.zeros(len(qids), len(batch_record.positions)).to(self.device)
        masks = torch.zeros(len(qids), len(batch_record.positions)).to(self.device)
        for i in range(len(qids)):
            for p in trees[i].treepositions('postorder')[:-1]:
                if type(trees[i][p]) is str:
                    continue
                idx = batch_record.positions.index(p)
                masks[i, idx] = 1

            for l in labels[i]:
                idx = batch_record.positions.index(tuple(l))
                logits[i, idx] = 1
        batch_record.labels = logits
        batch_record.masks = masks
        return batch_record


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Multiple Choice Reading Comprehension')
    parser.add_argument('--task', type=str, default='multirc',
                        help='task name')
    parser.add_argument('--model', type=str, default='BatchTree',
                        help='model name')
    parser.add_argument('--emb_dim', type=int, default=300,
                        help='size of word embeddings')
    parser.add_argument('--mem_dim', type=int, default=150,
                        help='hidden memory size')
    parser.add_argument('--lr', type=float, default=0.002,
                        help='initial learning rate')
    parser.add_argument('--epochs', type=int, default=50,
                        help='upper epoch limit')
    parser.add_argument('--batch_size', type=int, default=10, metavar='N',
                        help='batch size')
    parser.add_argument('--dropoutP', type=float, default=0.2,
                        help='dropout ratio')
    parser.add_argument('--seed', type=int, default=1111,
                        help='random seed')
    parser.add_argument('--cuda', action='store_true',
                        help='use CUDA')
    parser.add_argument('--interval', type=int, default=200, metavar='N',
                        help='report interval')
    parser.add_argument('--exp_idx', type=str, default='1',
                        help='experiment index')
    parser.add_argument('--log', type=str, default='nothing',
                        help='take note')
    parser.add_argument('--option2fact', action='store_true',
                        help='task name')
    args = parser.parse_args()

    model_dir = os.path.join('data', args.task, args.model)
    if not os.path.isdir(model_dir):
        os.makedirs(model_dir)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    corpus = Corpus(args.task, device)
