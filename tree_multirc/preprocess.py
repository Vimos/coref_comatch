import itertools
import json
import os
import re
import pprint

import numpy as np
# from allennlp.data.tokenizers import WordTokenizer
# from allennlp.predictors import Predictor
#
# predictor = Predictor.from_path("/home/vimos/git/Coref/coref-model-2018.02.05.tar.gz")
#
# tok = WordTokenizer()
from nltk import Tree
from stanfordcorenlp import StanfordCoreNLP

file_path = os.path.dirname(__file__)
multirc_dir = os.path.join(file_path, '..', 'data', 'multirc')
json_dir = os.path.join(multirc_dir, 'multirc-v2', 'splitv2')

text_re = re.compile('<b>Sent \d+: </b>(.*?)<br>')
nlp = StanfordCoreNLP(path_or_host='/home/vimos/Data/Dataset/CoreNLP/stanford-corenlp-full', memory='8g')
props = {'annotators': 'ssplit,dcoref,parse', 'pipelineLanguage': 'en', 'outputFormat': 'json'}


# def word_tokenize(text):
#     return [c.text for c in tok.tokenize(text)]
#
#
# def rindex(mylist, myvalue):
#     return len(mylist) - mylist[::-1].index(myvalue) - 1
#
#
# def pairwise(iterable):
#     """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
#     a, b = itertools.tee(iterable)
#     next(b, None)
#     return zip(a, b)
#
#
# def coreference(article):
#     d = {"document": ' '.join([' '.join(s) for s in article])}
#     results = predictor.predict_json(d)
#     # print('====================================================================')
#     # print('---------------------------------------------------------------------')
#     # for i, span in enumerate(results['top_spans']):
#     #     print('Span {}: {}'.format(i, ' '.join(results['document'][span[0]:span[1] + 1])))
#     # print('---------------------------------------------------------------------')
#     # for i, cluster in enumerate(results['clusters']):
#     #     for w in cluster:
#     #         print(' '.join(results['document'][w[0]:w[1] + 1]), end=', ')
#     #     print()
#
#     d = results['document']
#     d_len = list(map(len, article))
#     d_len.insert(0, 0)
#     start_len = list(pairwise(np.cumsum(d_len).tolist()))
#
#     # print("Total length: {} vs {}".format(sum(d_len), len(d)))
#     # print(len(results['clusters']))
#     # diff_len = abs(sum(d_len) - len(d))
#     clusters = []
#     for cluster in results['clusters']:
#         c = []
#         for w in cluster:
#             new_w = None
#             for k, (i, j) in enumerate(start_len):
#                 if i <= w[0] < j:
#                     new_i, new_j = w[0] - i, w[1] - i
#                     while True:
#                         try:
#                             if d[w[0]] == article[k][new_i]:
#                                 if d[w[1]] == article[k][new_j]:
#                                     new_w = (k, new_i, new_j)
#                                     break
#                             new_i = article[k].index(d[w[0]])
#                             new_j = article[k].index(d[w[1]])
#                         except Exception:
#                             print(d[w[0]:w[1] + 1])
#                             print(article[k][new_i:new_j + 1])
#                             break
#             if new_w:
#                 c.append(new_w)
#         if c:
#             clusters.append(c)
#
#     return clusters
#
#
# def clean_text(text):
#     # return text.replace("''", '" ').replace("``", '" ')
#     return text


def parse_para(para):
    sentences = [s.strip() for s in text_re.findall(para['paragraph']["text"])]

    annotation = json.loads(nlp.annotate(' '.join(sentences), properties=props))

    for i, sentence in enumerate(annotation['sentences']):
        tr = Tree.fromstring(sentence['parse'])
        tr.collapse_unary(collapsePOS=True)
        tr.chomsky_normal_form()
        annotation['sentences'][i]['tree'] = tr.pformat()
    return annotation


def parse_question_option(question_option):
    annotation = json.loads(nlp.annotate(question_option, properties=props))

    for i, sentence in enumerate(annotation['sentences']):
        tr = Tree.fromstring(sentence['parse'])
        tr.collapse_unary(collapsePOS=True)
        tr.chomsky_normal_form()
        annotation['sentences'][i]['tree'] = tr.pformat()
    return annotation


def preprocess():
    dataset_names = ['train', 'dev', 'test']
    dev_json = os.path.join(json_dir, 'dev_83-fixedIds.json')
    train_json = os.path.join(json_dir, 'train_456-fixedIds.json')
    q_id = 0
    for dataset_name, data_json in zip(dataset_names, [train_json, dev_json, dev_json]):
        data_all = []
        id_map = {}
        with open(data_json, "r") as fh:
            data_raw = json.load(fh)
            for para in data_raw['data']:

                filename = para['id']
                json_name = os.path.join(multirc_dir, 'annotations', '{}.json'.format(filename))

                if os.path.isfile(json_name):
                    with open(json_name, 'r') as fpw:
                        annotation = json.load(fpw)
                else:
                    parent_dir = os.path.dirname(json_name)
                    if not os.path.isdir(parent_dir):
                        os.makedirs(parent_dir)
                    annotation = parse_para(para)
                    with open(json_name, 'w', encoding='utf-8') as fpw:
                        json.dump(annotation, fpw, indent=2)
                for sentence in annotation['sentences']:
                    tr = Tree.fromstring(sentence['tree'])
                    print(' '.join(tr.leaves()))

                for idx, qa in enumerate(para['paragraph']['questions']):
                    for i, option in enumerate(qa['answers']):
                        instance = {
                            'ground_truth': int(option['isAnswer']),
                            'question_option': parse_question_option(qa["question"] + ' ' + option['text']),
                            'article': annotation,
                            'q_id': q_id,
                            'filename': filename
                        }
                        data_all.append(instance)
                        q_id += 1
                        id_map[q_id] = {
                            'pid': para['id'],
                            'qid': str(idx)
                        }
                        if len(data_all) % 1000 == 0:
                            print(len(data_all))
        with open(os.path.join(multirc_dir, 'sequence', '{}.json'.format(dataset_name)), 'w', encoding='utf-8') as fpw:
            json.dump(data_all, fpw, indent=2)
        with open(os.path.join(multirc_dir, 'sequence', '{}_map.json'.format(dataset_name)), 'w',
                  encoding='utf-8') as fpw:
            json.dump(id_map, fpw, indent=2)


if __name__ == '__main__':
    preprocess()
    nlp.close()
