'''
Copyright 2015 Singapore Management University (SMU). All Rights Reserved.
Permission to use, copy, modify and distribute this software and its documentation for purposes of research, teaching and general academic pursuits, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright statement, this paragraph and the following paragraph on disclaimer appear in all copies, modifications, and distributions.  Contact Singapore Management University, Intellectual Property Management Office at iie@smu.edu.sg, for commercial licensing opportunities.
This software is provided by the copyright holder and creator "as is" and any express or implied warranties, including, but not Limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed.  In no event shall SMU or the creator be liable for any direct, indirect, incidental, special, exemplary or consequential damages, however caused arising in any way out of the use of this software.
'''

from utils.func import *


class CoFlow(nn.Module):
    def __init__(self, corpus, args):
        super(CoFlow, self).__init__()
        self.emb_dim = 300
        self.mem_dim = args.mem_dim
        self.dropoutP = args.dropoutP
        self.cuda_bool = args.cuda
        self.args = args

        self.embs = nn.Embedding(len(corpus.dictionary), self.emb_dim)
        self.embs.weight.data.copy_(corpus.dictionary.embs)
        self.embs.weight.requires_grad = False

        self.encoder = MaskLSTM(self.emb_dim, self.mem_dim, dropoutP=self.dropoutP)

        self.match_module = MatchNet(self.mem_dim * 2, self.dropoutP)
        self.if_module = IntegrationFlow(self.mem_dim * 8, self.mem_dim,
                                         dropout=self.dropoutP)

        self.rank_module = nn.Linear(self.mem_dim * 4, 1)

        self.drop_module = nn.Dropout(self.dropoutP)

    def forward(self, inputs):
        d_word, d_h_len, d_l_len = inputs.documents
        o_word, o_h_len, o_l_len = inputs.options
        q_word, q_len = inputs.questions

        if self.cuda_bool:
            (d_word, d_h_len, d_l_len, o_word, o_h_len, o_l_len, q_word,
             q_len) = (
                d_word.cuda(), d_h_len.cuda(), d_l_len.cuda(), o_word.cuda(), o_h_len.cuda(), o_l_len.cuda(),
                q_word.cuda(),
                q_len.cuda())
        d_embs = self.drop_module(Variable(self.embs(d_word), requires_grad=False))
        o_embs = self.drop_module(Variable(self.embs(o_word), requires_grad=False))
        q_embs = self.drop_module(Variable(self.embs(q_word), requires_grad=False))

        n, s_n, dw_n, _ = d_embs.size()
        _, o_n, ow_n, _ = o_embs.size()

        d_hidden = self.encoder(
            [d_embs.view(n * s_n, dw_n, self.emb_dim), d_l_len.view(-1)])
        o_hidden = self.encoder(
            [o_embs.view(n * o_n, ow_n, self.emb_dim), o_l_len.view(-1)])
        q_hidden = self.encoder([q_embs, q_len])

        d_hidden_3d = d_hidden.view(n, s_n * dw_n, d_hidden.size(-1))
        d_hidden_3d_repeat = d_hidden_3d.repeat(1, o_n, 1).view(n * o_n,
                                                                s_n * dw_n,
                                                                d_hidden_3d.size(2))

        do_match = self.match_module([d_hidden_3d_repeat, o_hidden, o_l_len.view(-1)])
        dq_match = self.match_module([d_hidden_3d, q_hidden, q_len])

        dq_match_repeat = dq_match.repeat(1, o_n, 1).view(n * o_n,
                                                          s_n * dw_n,
                                                          dq_match.size(2))

        co_match = torch.cat([do_match, dq_match_repeat], -1)

        if1 = self.if_module([co_match.view(n, o_n, s_n, dw_n, -1), d_l_len, o_l_len])

        h_hidden_pool, _ = if1.view(n * o_n, s_n * dw_n, -1).max(1)

        o_rep = h_hidden_pool.view(n, o_n, -1)
        # output = torch.nn.functional.log_softmax(self.rank_module(o_rep).squeeze(2))
        output = masked_logits(self.rank_module(o_rep), torch.unsqueeze(o_l_len > 0, -1)).squeeze(2)
        return output
