import itertools
import json
import os
import re

import numpy as np
from allennlp.data.tokenizers import WordTokenizer
from allennlp.predictors import Predictor

from text_utils import TextEncoder

file_path = os.path.dirname(__file__)
multirc_dir = os.path.join(file_path, '..', 'data', 'multirc')
json_dir = os.path.join(multirc_dir, 'multirc-v2', 'splitv2')
predictor = Predictor.from_path("/home/vimos/git/Coref/coref-model-2018.02.05.tar.gz")

tok = WordTokenizer()


def word_tokenize(text):
    return [c.text for c in tok.tokenize(text)]


def preprocess():
    dataset_names = ['train', 'dev']
    dev_json = os.path.join(json_dir, 'dev_83-fixedIds.json')
    train_json = os.path.join(json_dir, 'train_456-fixedIds.json')
    text_re = re.compile('<b>Sent \d+: </b>(.*?)<br>')

    data = {}
    for dataset_name, data_json in zip(dataset_names, [train_json, dev_json]):
        articles, questions, options, ground_truths = [], [], [], []
        with open(data_json, "r") as fh:
            data_raw = json.load(fh)
            for para in data_raw['data']:
                sentences = [s.strip() for s in
                             text_re.findall(para['paragraph']["text"])]

                article = [' '.join(word_tokenize(s)) for s in sentences]

                filename = para['id']
                for idx, qa in enumerate(para['paragraph']['questions']):
                    question = qa["question"]
                    options_list = [word_tokenize(answer['text']) for answer in qa['answers']]
                    ground_truth_list = [int(answer['isAnswer']) for answer in qa['answers']]

                    for option, ground_truth in zip(options_list, ground_truth_list):
                        articles.append(' '.join(article))
                        questions.append(question)
                        options.append(' '.join(option))
                        ground_truths.append(ground_truth)
        data[dataset_name] = (articles, questions, options, ground_truths)
    return data['train'], data['dev']


def encode_dataset(*splits, encoder):
    encoded_splits = []
    for split in splits:
        fields = []
        for field in split:
            if isinstance(field[0], str):
                field = encoder.encode(field)
            fields.append(field)
        encoded_splits.append(fields)
    return encoded_splits


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Multiple Choice Reading Comprehension')
    parser.add_argument('--encoder_path', type=str, default='model/encoder_bpe_40000.json')
    parser.add_argument('--bpe_path', type=str, default='model/vocab_40000.bpe')
    args = parser.parse_args()

    text_encoder = TextEncoder(args.encoder_path, args.bpe_path)
    encoder = text_encoder.encoder
    n_vocab = len(text_encoder.encoder)

    print("Encoding dataset...")
    ((trX1, trX2, trX3, trY),
     (vaX1, vaX2, vaX3, vaY)) = encode_dataset(*preprocess(),
                                               encoder=text_encoder)

    print(vaX1)
