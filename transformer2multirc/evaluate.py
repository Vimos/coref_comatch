"""
Copyright 2015 Singapore Management University (SMU). All Rights Reserved.
Permission to use, copy, modify and distribute this software and its documentation for purposes of research, teaching and general academic pursuits, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright statement, this paragraph and the following paragraph on disclaimer appear in all copies, modifications, and distributions.  Contact Singapore Management University, Intellectual Property Management Office at iie@smu.edu.sg, for commercial licensing opportunities.
This software is provided by the copyright holder and creator "as is" and any express or implied warranties, including, but not Limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed.  In no event shall SMU or the creator be liable for any direct, indirect, incidental, special, exemplary or consequential damages, however caused arising in any way out of the use of this software.
"""
import json
import math
import os
import torch
from functools import reduce

import numpy


def avg(l):
    return reduce(lambda x, y: x + y, l) / len(l)


file_path = os.path.dirname(__file__)
multirc_dir = os.path.join(file_path, '..', 'data', 'multirc')
json_dir = os.path.join(multirc_dir, 'multirc-v2', 'splitv2')
input_file = os.path.join(json_dir, 'dev_83-fixedIds.json')


class Measures:

    def __init__(self, mode, threshold=None):
        """

        :param mode: binary threshold reciprocal
        :param threshold:
        """
        self.mode = mode
        self.threshold = threshold

    def get_prediction(self, output):
        if self.mode == 'binary':
            predictedAns = output
        elif self.mode == 'threshold':
            predictedAns = [a > self.threshold for a in output]
        else:
            predictedAns = [a > math.log(1 / len(output)) for a in output]
        return predictedAns

    def per_question_metrics(self, dataset, output_map):
        P = []
        R = []
        for p in dataset:
            for qIdx, q in enumerate(p["paragraph"]["questions"]):
                idx = p["id"] + "==" + str(qIdx)
                if (idx in output_map):
                    predictedAns = self.get_prediction(output_map.get(idx))
                    correctAns = [int(a["isAnswer"]) for a in q["answers"]]
                    predictCount = sum(predictedAns)
                    correctCount = sum(correctAns)
                    assert math.ceil(sum(predictedAns)) == sum(predictedAns), "sum of the scores: " + str(
                        sum(predictedAns))
                    agreementCount = sum([a * b for (a, b) in zip(correctAns, predictedAns)])
                    p1 = (1.0 * agreementCount / predictCount) if predictCount > 0.0 else 1.0
                    r1 = (1.0 * agreementCount / correctCount) if correctCount > 0.0 else 1.0
                    P.append(p1)
                    R.append(r1)
                else:
                    print("The id " + idx + " not found . . . ")

        pAvg = Measures.avg(P)
        rAvg = Measures.avg(R)
        f1Avg = 2 * Measures.avg(R) * Measures.avg(P) / (Measures.avg(P) + Measures.avg(R))
        return [pAvg, rAvg, f1Avg]

    def exact_match_metrics(self, dataset, output_map, delta):
        EM = []
        for p in dataset:
            for qIdx, q in enumerate(p["paragraph"]["questions"]):
                idx = p["id"] + "==" + str(qIdx)
                if (idx in output_map):
                    predictedAns = self.get_prediction(output_map.get(idx))
                    correctAns = [int(a["isAnswer"]) for a in q["answers"]]
                    em = 1.0 if sum([abs(i - j) for i, j in zip(correctAns, predictedAns)]) <= delta else 0.0
                    EM.append(em)
                else:
                    print("The id " + idx + " not found . . . ")

        return Measures.avg(EM)

    def per_dataset_metric(self, dataset, output_map):
        agreementCount = 0
        correctCount = 0
        predictCount = 0
        for p in dataset:
            for qIdx, q in enumerate(p["paragraph"]["questions"]):
                idx = p["id"] + "==" + str(qIdx)
                if (idx in output_map):
                    predictedAns = self.get_prediction(output_map.get(idx))
                    correctAns = [int(a["isAnswer"]) for a in q["answers"]]
                    predictCount += sum(predictedAns)
                    correctCount += sum(correctAns)
                    agreementCount += sum([a * b for (a, b) in zip(correctAns, predictedAns)])
                else:
                    print("The id " + idx + " not found . . . ")

        p1 = (1.0 * agreementCount / predictCount) if predictCount > 0.0 else 1.0
        r1 = (1.0 * agreementCount / correctCount) if correctCount > 0.0 else 1.0
        return [p1, r1, 2 * r1 * p1 / (p1 + r1)]

    @staticmethod
    def avg(l):
        return reduce(lambda x, y: x + y, l) / len(l)


def evaluate(out_file, thr, mode='threshold'):
    measures = Measures(mode=mode, threshold=thr)

    input_data = json.load(open(input_file))
    output = json.load(open(out_file))
    output_map = dict([[a["pid"] + "==" + a["qid"], a["scores"]] for a in output])

    assert len(output_map) == len(output), "You probably have redundancies in your keys"

    [P1, R1, F1m] = measures.per_question_metrics(input_data["data"], output_map)
    print("Per question measures (i.e. precision-recall per question, then average) ")
    print("\tP: " + str(P1) + " - R: " + str(R1) + " - F1m: " + str(F1m))

    EM0 = measures.exact_match_metrics(input_data["data"], output_map, 0)
    EM1 = measures.exact_match_metrics(input_data["data"], output_map, 1)
    print("\tEM0: " + str(EM0))
    print("\tEM1: " + str(EM1))

    [P2, R2, F1a] = measures.per_dataset_metric(input_data["data"], output_map)

    print("Dataset-wide measures (i.e. precision-recall across all the candidate-answers in the dataset) ")
    print("\tP: " + str(P2) + " - R: " + str(R2) + " - F1a: " + str(F1a))
    return (P1, R1, F1m), (P2, R2, F1a), (EM0, EM1)


def evaluation(model, summary_writer, criterion, corpus, cuda, batch_size, dataset='dev', iteration=0):
    model.eval()
    total_loss = 0
    count = 0
    with open(os.path.join(multirc_dir, 'sequence', '{}_map.json'.format(dataset)),
              'r', encoding='utf-8') as fpw:
        id_map = json.load(fpw)

    result = []
    while True:
        data = corpus.get_batch(batch_size, dataset)
        output = model(data)
        labels, labels_len = data.labels
        qids = data.qids
        labels = labels.cuda() if cuda else labels
        loss = criterion(output, labels)

        loss = loss.detach()
        total_loss += float(loss.item())
        count += output.size(0)

        for label, label_len, o, qid in zip(labels, labels_len, output, qids):
            d = id_map[str(qid)]
            d['scores'] = o.detach().cpu().numpy().tolist()[:label_len.item()]
            result.append(d)

        if corpus.start_id[dataset] >= len(corpus.data_all[dataset]):
            break

    loss = total_loss / count

    output_file = os.path.join(multirc_dir, model.args.model, '{}_result.json'.format(dataset))
    with open(output_file, 'w', encoding='utf-8') as fpw:
        json.dump(result, fpw, indent=2)

    min_val = min([min(a["scores"]) for a in result])
    max_val = max([max(a["scores"]) for a in result])
    max_f1m, max_f1a, max_thr, max_em0, max_em1 = 0, 0, 0, 0, 0
    PR = []
    for i, thr in enumerate(numpy.arange(min_val - 0.1, max_val + 0.1, (max_val - min_val) / 10)):
        print('-----------------------------threshold = {}---------------'.format(thr))
        (P1, R1, F1m), (P2, R2, F1a), (EM0, EM1) = evaluate(output_file, thr)
        if F1m > max_f1m:
            max_thr = thr
            max_f1m = F1m
            max_f1a = F1a
            max_em0 = EM0
            max_em1 = EM1
            PR = [(P1, R1), (P2, R2)]

    # summary_writer.add_pr_curve('{}/pr'.format(dataset),
    #                             torch.cat(l),
    #                             torch.exp(torch.cat(p)),
    #                             iteration)

    summary_writer.add_scalar('{}/loss'.format(dataset), loss, iteration)
    summary_writer.add_scalar('{}/thr'.format(dataset), max_thr, iteration)
    summary_writer.add_scalars('{}/metrics'.format(dataset),
                               {'F1m': max_f1m,
                                'P1': PR[0][0],
                                'R1': PR[0][1],
                                'P2': PR[1][0],
                                'R2': PR[1][1],
                                'EM0': max_em0,
                                'EM1': max_em1,
                                'F1a': max_f1a},
                               iteration)
    model.train()
    return max_f1m


if __name__ == '__main__':
    output_file = os.path.join(multirc_dir, 'CoRef', 'test_result.json')
    evaluate(output_file, thr=-0.1, mode='reciprocal')
