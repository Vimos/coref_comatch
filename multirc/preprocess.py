import itertools
import json
import os
import re

import numpy as np
from allennlp.data.tokenizers import WordTokenizer
from allennlp.predictors import Predictor

file_path = os.path.dirname(__file__)
multirc_dir = os.path.join(file_path, '..', 'data', 'multirc')
json_dir = os.path.join(multirc_dir, 'multirc-v2', 'splitv2')
predictor = Predictor.from_path("/home/vimos/git/Coref/coref-model-2018.02.05.tar.gz")

tok = WordTokenizer()


def word_tokenize(text):
    return [c.text for c in tok.tokenize(text)]


def rindex(mylist, myvalue):
    return len(mylist) - mylist[::-1].index(myvalue) - 1


def pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def coreference(article):
    d = {"document": ' '.join([' '.join(s) for s in article])}
    results = predictor.predict_json(d)
    # print('====================================================================')
    # print('---------------------------------------------------------------------')
    # for i, span in enumerate(results['top_spans']):
    #     print('Span {}: {}'.format(i, ' '.join(results['document'][span[0]:span[1] + 1])))
    # print('---------------------------------------------------------------------')
    # for i, cluster in enumerate(results['clusters']):
    #     for w in cluster:
    #         print(' '.join(results['document'][w[0]:w[1] + 1]), end=', ')
    #     print()

    d = results['document']
    d_len = list(map(len, article))
    d_len.insert(0, 0)
    start_len = list(pairwise(np.cumsum(d_len).tolist()))

    # print("Total length: {} vs {}".format(sum(d_len), len(d)))
    # print(len(results['clusters']))
    # diff_len = abs(sum(d_len) - len(d))
    clusters = []
    for cluster in results['clusters']:
        c = []
        for w in cluster:
            new_w = None
            for k, (i, j) in enumerate(start_len):
                if i <= w[0] < j:
                    new_i, new_j = w[0] - i, w[1] - i
                    while True:
                        try:
                            if d[w[0]] == article[k][new_i]:
                                if d[w[1]] == article[k][new_j]:
                                    new_w = (k, new_i, new_j)
                                    break
                            new_i = article[k].index(d[w[0]])
                            new_j = article[k].index(d[w[1]])
                        except Exception:
                            print(d[w[0]:w[1] + 1])
                            print(article[k][new_i:new_j + 1])
                            break
            if new_w:
                c.append(new_w)
        if c:
            clusters.append(c)

    return clusters


def clean_text(text):
    # return text.replace("''", '" ').replace("``", '" ')
    return text


def preprocess():
    dataset_names = ['train', 'dev', 'test']
    dev_json = os.path.join(json_dir, 'dev_83-fixedIds.json')
    train_json = os.path.join(json_dir, 'train_456-fixedIds.json')
    text_re = re.compile('<b>Sent \d+: </b>(.*?)<br>')
    q_id = 0
    for dataset_name, data_json in zip(dataset_names, [train_json, dev_json, dev_json]):
        data_all = []
        id_map = {}
        with open(data_json, "r") as fh:
            data_raw = json.load(fh)
            for para in data_raw['data']:
                sentences = [s.strip() for s in
                             text_re.findall(clean_text(para['paragraph']["text"]))]

                article = [word_tokenize(s) for s in sentences]
                clusters = coreference(article)

                filename = para['id']
                for idx, qa in enumerate(para['paragraph']['questions']):
                    question = clean_text(qa["question"])
                    options = [word_tokenize(answer['text']) for answer in qa['answers']]
                    ground_truth = [int(answer['isAnswer']) for answer in qa['answers']]
                    instance = {
                        'ground_truth': ground_truth,
                        'options': options,
                        'question': word_tokenize(question),
                        'article': article,
                        'clusters': clusters,
                        'q_id': q_id,
                        'filename': filename
                    }
                    data_all.append(instance)
                    id_map[q_id] = {
                        'pid': para['id'],
                        'qid': str(idx)
                    }
                    q_id += 1
                    if len(data_all) % 1000 == 0:
                        print(len(data_all))
        with open(os.path.join(multirc_dir, 'sequence', dataset_name) + '.json', 'w', encoding='utf-8') as fpw:
            json.dump(data_all, fpw, indent=2)
        with open(os.path.join(multirc_dir, 'sequence', dataset_name) + '_map.json', 'w',
                  encoding='utf-8') as fpw:
            json.dump(id_map, fpw, indent=2)


if __name__ == '__main__':
    preprocess()
