import glob
import itertools
import json
import os
import re
import numpy as np
from nltk import sent_tokenize, word_tokenize

# import spacy
from allennlp.data.tokenizers import WordTokenizer
from allennlp.predictors import Predictor

# from spacy.tokenizer import Tokenizer

file_path = os.path.dirname(__file__)
race_dir = os.path.join(file_path, '..', 'data', 'race')
predictor = Predictor.from_path("/home/vimos/git/Coref/coref-model-2018.02.05.tar.gz")

tok = WordTokenizer()
label_dict = {'A': 0, 'B': 1, 'C': 2, 'D': 3}


# nlp = spacy.load('en_core_web_lg')
# tokenizer = Tokenizer(nlp.vocab)


# def word_tokenize(text):
#     r = []
#     for w in tok.tokenize(text):
#         if w.text in ["they'll", "he'll"]:
#             w1, _ = w.text.split("'")
#             r.append(w1)
#             r.append("'ll")
#         else:
#             r.append(w.text)
#     return r


def rindex(mylist, myvalue):
    return len(mylist) - mylist[::-1].index(myvalue) - 1


def pairwise(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def untokenize(words):
    """
    Untokenizing a text undoes the tokenizing operation, restoring
    punctuation and spaces to the places that people expect them to be.
    Ideally, `untokenize(tokenize(text))` should be identical to `text`,
    except for line breaks.
    """
    text = ' '.join(words)
    step1 = text.replace("`` ", '"').replace(" ''", '"').replace('. . .', '...')
    step2 = step1.replace(" ( ", " (").replace(" ) ", ") ")
    step3 = re.sub(r' ([.,:;?!%]+)([ \'"`])', r"\1\2", step2)
    step4 = re.sub(r' ([.,:;?!%]+)$', r"\1", step3)
    step5 = step4.replace(" '", "'").replace(" n't", "n't").replace(
        "can not", "cannot")
    step6 = step5.replace(" ` ", " '")
    return step6.strip()


def coreference(article_txt):
    article_txt = article_txt.replace("\n", " ").replace(".", ". ")
    article = [word_tokenize(s) for s in sent_tokenize(article_txt)]
    article_txt = ' '.join([' '.join(s) for s in article]).replace("''", '"').replace("``", '"').replace('\n', '')
    article = [[t.text for t in tok.tokenize(s)] for s in sent_tokenize(article_txt)]
    # article_txt = ' '.join([untokenize(s) for s in article])
    try:
        results = predictor.predict(article_txt)
    except:
        return [], article

    d = results['document']
    d_len = list(map(len, article))
    diff_max = abs(sum(d_len) - len(d))
    diff = []

    start = 0
    for i, s in enumerate(article):
        right = start
        s_len = len(s)
        j_list = []
        while True:
            sublist = d[right:]
            anchor = '"' if s[-1] in ["''", "``"] else s[-1]
            j = sublist.index(anchor)
            j_list.append(j + 1)
            if abs(j + 1 - s_len) <= abs(sum(diff)):
                break
            else:
                right += (j + 1)
                s_len -= (j + 1)
        l_i = sum(j_list)
        d_len[i] = l_i
        article[i] = d[start:start + l_i]
        start += l_i
        diff.append(len(s) - len(article[i]))

    d_len.insert(0, 0)
    start_len = list(pairwise(np.cumsum(d_len).tolist()))

    clusters = []
    for cluster in results['clusters']:
        c = []
        try:
            for w in cluster:
                for k, (i, j) in enumerate(start_len):
                    if i <= w[0] < j:
                        new_i, new_j = w[0] - i, w[1] - i
                        os = d[w[0]:w[1] + 1]
                        ns = article[k][new_i:new_j + 1]
                        if os == ns:
                            c.append((k, new_i, new_j))
                            break
        except Exception:
            print(article_txt)
        if c:
            clusters.append(c)

    return clusters, article


def sent_word_tokenize(text):
    sents = sent_tokenize(text)
    words = [word_tokenize(s) for s in sents]
    return words


def parse_file(filename, dataset_name, level_name):
    inter_file = os.path.join(race_dir, 'intermediate', dataset_name, level_name, '{}.json'.format(filename.name))
    if os.path.isfile(inter_file):
        with open(inter_file, 'r', encoding='utf-8') as fpw:
            instances = json.load(fpw)
    else:
        with open(filename.path, 'r', encoding='utf-8') as fpr:
            data_raw = json.load(fpr)
            # article = [word_tokenize(s.strip()) for s in sent_tokenize(data_raw['article'])]
            clusters, article = coreference(data_raw['article'])
            instances = []
            for i in range(len(data_raw['answers'])):
                instance = {'ground_truth': label_dict[data_raw['answers'][i]],
                            'options': [word_tokenize(option) for option in data_raw['options'][i]],
                            'question': word_tokenize(data_raw['questions'][i]),
                            'article': article,
                            'clusters': clusters,
                            'filename': filename.path}
                instances.append(instance)
            with open(inter_file, 'w', encoding='utf-8') as fpw:
                json.dump(instances, fpw)
    return instances


def preprocess():
    dataset_names = ['train', 'dev', 'test']
    level_names = ['high', 'middle']
    q_id = 0

    for dataset_name in dataset_names:
        data_all = []
        for level_name in level_names:
            if not os.path.isdir(os.path.join(race_dir, 'intermediate', dataset_name, level_name)):
                os.makedirs(os.path.join(race_dir, 'intermediate', dataset_name, level_name))
            path = os.path.join(race_dir, 'RACE', dataset_name, level_name)
            for filename in os.scandir(path):
                instances = parse_file(filename, dataset_name, level_name)
                for instance in instances:
                    instance['q_id'] = q_id
                    data_all.append(instance)
                    q_id += 1
                    if q_id % 1000 == 0:
                        print(q_id)
        with open(os.path.join(race_dir, 'sequence', '{}.json'.format(dataset_name)), 'w', encoding='utf-8') as fpw:
            json.dump(data_all, fpw)


if __name__ == '__main__':
    preprocess()
