# coding: utf-8
'''
Copyright 2015 Singapore Management University (SMU). All Rights Reserved.
Permission to use, copy, modify and distribute this software and its documentation for purposes of research, teaching and general academic pursuits, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright statement, this paragraph and the following paragraph on disclaimer appear in all copies, modifications, and distributions.  Contact Singapore Management University, Intellectual Property Management Office at iie@smu.edu.sg, for commercial licensing opportunities.
This software is provided by the copyright holder and creator "as is" and any express or implied warranties, including, but not Limited to, the implied warranties of merchantability and fitness for a particular purpose are disclaimed.  In no event shall SMU or the creator be liable for any direct, indirect, incidental, special, exemplary or consequential damages, however caused arising in any way out of the use of this software.
'''
import argparse
import os
import time
from datetime import datetime

import torch
import torch.nn as nn
import torch.optim as optim
from tensorboardX import SummaryWriter

from utils.corpus import Corpus

parser = argparse.ArgumentParser(description='Multiple Choice Reading Comprehension')
parser.add_argument('--task', type=str, default='multirc',
                    help='task name')
parser.add_argument('--model', type=str, default='CoMatch',
                    help='model name')
parser.add_argument('--emb_dim', type=int, default=300,
                    help='size of word embeddings')
parser.add_argument('--mem_dim', type=int, default=150,
                    help='hidden memory size')
parser.add_argument('--lr', type=float, default=0.002,
                    help='initial learning rate')
parser.add_argument('--epochs', type=int, default=50,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=10, metavar='N',
                    help='batch size')
parser.add_argument('--dropoutP', type=float, default=0.3,
                    help='dropout ratio')
parser.add_argument('--seed', type=int, default=1111,
                    help='random seed')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--interval', type=int, default=200, metavar='N',
                    help='report interval')
parser.add_argument('--exp_idx', type=str, default='1',
                    help='experiment index')
parser.add_argument('--log', type=str, default='nothing',
                    help='take note')
parser.add_argument('--option2fact',  action='store_true',
                    help='task name')
parser.add_argument('--n_ctx', type=int, default=512)
parser.add_argument('--n_embd', type=int, default=768)
parser.add_argument('--n_head', type=int, default=12)
parser.add_argument('--n_layer', type=int, default=12)
parser.add_argument('--embd_pdrop', type=float, default=0.1)
parser.add_argument('--attn_pdrop', type=float, default=0.1)
parser.add_argument('--resid_pdrop', type=float, default=0.1)
parser.add_argument('--clf_pdrop', type=float, default=0.1)
parser.add_argument('--afn', type=str, default='gelu')
parser.add_argument('--encoder_path', type=str, default='model/encoder_bpe_40000.json')
parser.add_argument('--bpe_path', type=str, default='model/vocab_40000.bpe')
args = parser.parse_args()

torch.manual_seed(args.seed)
if torch.cuda.is_available():
    if not args.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")
    else:
        torch.cuda.manual_seed(args.seed)

if args.task == 'race':
    from race.evaluate import evaluation

    criterion = nn.NLLLoss()
elif args.task == 'transformer2multirc':
    from transformer2multirc.evaluate import evaluation

    criterion = nn.NLLLoss()
    # criterion = nn.BCEWithLogitsLoss()
else:
    from multirc.evaluate import evaluation

    criterion = nn.BCEWithLogitsLoss()
    # criterion = nn.MultiLabelMarginLoss()

if args.model == 'CoMatch':
    from comatch import CoMatch as Model
elif args.model == 'Transformer2CoMatch':
    from transformer2comatch import Transformer2CoMatch as Model
elif args.model == 'CoFlow':
    from coflow import CoFlow as Model
else:
    from coref import CoRef as Model

model_dir = os.path.join('data', args.task, args.model)
if not os.path.isdir(model_dir):
    os.makedirs(model_dir)

corpus = Corpus(args.task, args.option2fact)
exec_time = datetime.now().strftime("%Y-%m-%d-%H-%M")
print(exec_time)
writer = SummaryWriter(log_dir=os.path.join(model_dir, 'logs', exec_time))

model = Model(corpus, args)
parameters = filter(lambda p: p.requires_grad, model.parameters())
optimizer = optim.Adamax(parameters, lr=args.lr)

if args.cuda:
    model.cuda()
    criterion.cuda()

start_time = time.time()
total_loss = 0
interval = args.interval
save_interval = len(corpus.data_all['train']) // args.batch_size


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


best_dev_score = -99999
iterations = args.epochs * len(corpus.data_all['train']) // args.batch_size
print('max iterations: ' + str(iterations))
print('Parameter count: {}'.format(count_parameters(model)))
model.train()
for itr in range(iterations):
    optimizer.zero_grad()

    data = corpus.get_batch(args.batch_size, 'train')
    output = model(data)

    labels, labels_len = data.labels
    labels = labels.cuda() if args.cuda else labels

    loss = criterion(output, labels)
    loss.backward()
    optimizer.step()

    writer.add_scalar('train/loss', loss.item(), itr)
    total_loss += loss.item()

    if itr % interval == 0:
        cur_loss = total_loss / interval if itr != 0 else total_loss
        elapsed = time.time() - start_time
        print('| iterations {:3d} | start_id {:3d} | ms/batch {:5.2f} | loss {:5.3f}'.format(
            itr, corpus.start_id['train'], elapsed * 1000 / interval, cur_loss))
        writer.add_scalar('train/cur_loss', cur_loss, itr)
        writer.add_scalar('train/elapsed', elapsed * 1000 / interval, itr)
        total_loss = 0
        start_time = time.time()

    if itr % save_interval == 0:

        torch.save([model, optimizer, criterion], os.path.join(model_dir, args.task + '_save.pt'))
        score = evaluation(model, writer, criterion, corpus, args.cuda, args.batch_size,
                           dataset='dev', iteration=itr)
        print('DEV accuracy: ' + str(score))

        with open(os.path.join(model_dir, args.task + '_record.txt'), 'a', encoding='utf-8') as fpw:
            if itr == 0:
                fpw.write(str(args) + '\n')
            fpw.write(str(itr) + ':\tDEV accuracy:\t' + str(score) + '\n')

        if score > best_dev_score:
            best_dev_score = score
            torch.save([model, optimizer, criterion], os.path.join(model_dir, args.task + '_save_best.pt'))

    if (itr + 1) % (len(corpus.data_all['train']) // args.batch_size) == 0:
        for param_group in optimizer.param_groups:
            param_group['lr'] *= 0.95
            writer.add_scalar('train/lr', param_group['lr'], itr)

model, optimizer, criterion = torch.load(os.path.join(model_dir, args.task + '_save_best.pt'))
score = evaluation(model, writer, criterion, corpus, args.cuda, args.batch_size, dataset='test')
with open(os.path.join(model_dir, args.task + '_record.txt'), 'a', encoding='utf-8') as fpw:
    fpw.write('TEST accuracy:\t' + str(score) + '\n')
print('TEST accuracy: ' + str(score))

# export scalar data to JSON for external processing
writer.export_scalars_to_json(os.path.join(model_dir, "all_scalars.json"))
writer.close()
