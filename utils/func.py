import torch
import torch.nn as nn
from torch.autograd import Variable


def masked_logits(logits, mask=None):
    eps = -1e30
    mask = mask.float()
    return logits * mask + eps * (1 - mask)


def masked_softmax(vector, seq_lens):
    mask = vector.new(vector.size()).zero_()
    for i in range(seq_lens.size(0)):
        mask[i, :, :seq_lens[i]] = 1
    mask = Variable(mask, requires_grad=False)

    if mask is None:
        result = torch.nn.functional.softmax(vector, dim=-1)
    else:
        result = torch.nn.functional.softmax(vector * mask, dim=-1)
        result = result * mask
        result = result / (result.sum(dim=-1, keepdim=True) + 1e-13)
    return result


class LayerNorm(nn.Module):
    "Construct a layernorm module in the OpenAI style (epsilon inside the square root)."

    def __init__(self, n_state, e=1e-5):
        super(LayerNorm, self).__init__()
        self.g = nn.Parameter(torch.ones(n_state))
        self.b = nn.Parameter(torch.zeros(n_state))
        self.e = e

    def forward(self, x):
        u = x.mean(-1, keepdim=True)
        s = (x - u).pow(2).mean(-1, keepdim=True)
        x = (x - u) / torch.sqrt(s + self.e)
        return self.g * x + self.b


class MatchNet(nn.Module):
    def __init__(self, mem_dim, dropoutP):
        super(MatchNet, self).__init__()
        self.map_linear = nn.Linear(2 * mem_dim, 2 * mem_dim)
        self.trans_linear = nn.Linear(mem_dim, mem_dim)
        self.drop_module = nn.Dropout(dropoutP)

    def forward(self, inputs):
        proj_p, proj_q, seq_len = inputs
        trans_q = self.trans_linear(proj_q)
        att_weights = proj_p.bmm(torch.transpose(proj_q, 1, 2))
        att_norm = masked_softmax(att_weights, seq_len)

        att_vec = att_norm.bmm(proj_q)
        elem_min = att_vec - proj_p
        elem_mul = att_vec * proj_p
        all_con = torch.cat([elem_min, elem_mul], 2)
        output = nn.ReLU()(self.map_linear(all_con))
        return output


class MaskLSTM(nn.Module):
    def __init__(self, in_dim, out_dim, layers=1, batch_first=True, bidirectional=True, dropoutP=0.3):
        super(MaskLSTM, self).__init__()
        self.lstm_module = nn.LSTM(in_dim, out_dim, layers, batch_first=batch_first, bidirectional=bidirectional,
                                   dropout=dropoutP)
        self.drop_module = nn.Dropout(dropoutP)

    def forward(self, inputs):

        input, seq_lens = inputs
        mask_in = input.new(input.size()).zero_()
        for i in range(seq_lens.size(0)):
            mask_in[i, :seq_lens[i]] = 1
        mask_in = Variable(mask_in, requires_grad=False)

        input_drop = self.drop_module(input * mask_in)

        # self.lstm_module.flatten_parameters()
        H, _ = self.lstm_module(input_drop)

        mask = H.new(H.size()).zero_()
        for i in range(seq_lens.size(0)):
            mask[i, :seq_lens[i]] = 1
        mask = Variable(mask, requires_grad=False)

        output = H * mask

        return output


class MaskGRU(nn.Module):
    def __init__(self, in_dim, out_dim, layers=1, batch_first=True, bidirectional=True, dropoutP=0.3):
        super(MaskGRU, self).__init__()
        self.gru_module = nn.GRU(in_dim, out_dim, layers, batch_first=batch_first, bidirectional=bidirectional,
                                 dropout=dropoutP)
        self.drop_module = nn.Dropout(dropoutP)

    def forward(self, inputs):

        input, seq_lens = inputs
        mask_in = input.new(input.size()).zero_()
        for i in range(seq_lens.size(0)):
            mask_in[i, :seq_lens[i]] = 1
        mask_in = Variable(mask_in, requires_grad=False)

        input_drop = self.drop_module(input * mask_in)

        # self.lstm_module.flatten_parameters()
        H, _ = self.gru_module(input_drop)

        mask = H.new(H.size()).zero_()
        for i in range(seq_lens.size(0)):
            mask[i, :seq_lens[i]] = 1
        mask = Variable(mask, requires_grad=False)

        output = H * mask

        return output


class IntegrationFlow(nn.Module):
    def __init__(self, in_dim, mem_dim, dropout=0.3):
        super(IntegrationFlow, self).__init__()
        self.drop_module = nn.Dropout(dropout)
        self.integration_module = MaskLSTM(in_dim, mem_dim, dropoutP=dropout)
        self.flow_module = MaskGRU(mem_dim * 2, mem_dim, dropoutP=dropout)

    def forward(self, inputs):
        c, context_lens, option_lens = inputs
        n, o_n, s_n, dw_n, _ = c.size()
        o_lens = (option_lens > 0).sum(1)

        c_tilde = self.integration_module([c.view(n * o_n * s_n, dw_n, -1),
                                           context_lens.repeat(1, o_n).view(-1)])

        c_tilde_transpose = c_tilde.view(n, o_n, s_n * dw_n, c_tilde.size(-1)).permute(0, 2, 1, 3)
        f_hat = self.flow_module([c_tilde_transpose.contiguous().view(n * s_n * dw_n, o_n, c_tilde_transpose.size(-1)),
                                  o_lens.repeat(1, s_n * dw_n).view(-1)])

        f = f_hat.view(n, s_n * dw_n, o_n, -1).permute(0, 2, 1, 3)
        return torch.cat([c_tilde.view(n, o_n, s_n * dw_n, c_tilde.size(-1)), f], -1).contiguous().view(n, o_n, s_n,
                                                                                                        dw_n, -1)
