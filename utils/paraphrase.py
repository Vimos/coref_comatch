import pandas as pda

names = [n.strip() for n in
         "LHS ||| PHRASE ||| PARAPHRASE ||| (FEATURE=VALUE )* ||| ALIGNMENT ||| ENTAILMENT".split("|||")]
print(names)
df = pda.read_csv('../data/ppdb/ppdb-2.0-tldr.gz',
                  sep=" \|\|\| ",
                  compression='gzip',
                  engine='python',
                  names=names)
print(df.head())
print(df.tail())
